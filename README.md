# Translation Library
![Version](https://img.shields.io/badge/version-develop-red.svg)
[![License](https://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT)
[![Language](https://img.shields.io/badge/language-Java%201.8-blue.svg)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## About
This library is a collection of Java classes that provide features for managing simple translation features for an
application. The idea is to setup a context with multiple providers that can be used for an application. The order
how the providers are added, is the priority how the text items are retrieved. So the last added provider is the
fallback language.

## Usage
### Example
``` java
  // setup a provider for german texts.
  final MemoryTranslationProvider german = new MemoryTranslationProvider();
  german.addTranslation("HELLO_WORLD", "Hallo Welt!");

  // setup a provider for english texts.
  final MemoryTranslationProvider english = new MemoryTranslationProvider();
  english.addTranslation("HELLO_WORLD", "Hello World!");

  // setup the translation context and add providers in the correct order.
  final TranslationContext context = TranslationContextFactory.createContext();
  context.addProvider(german);
  context.addProvider(english);

  System.out.println(context.translate("HELLO_WORLD"));
  // Output: Hallo Welt!
```

## Providers
<dl>
  <dt>EagerJsonTranslationProvider</dt>
  <dd>A provider that loads a simple JSON file into a hash map. The file will be loaded at a programmatically given time
    and stored in the memory with the <code>load()</code> method.</dd>

  <dt>EagerXmlTranslationProvider</dt>
  <dd>A provider that loads a XML file into a hash map. The file will be loaded at a programmatically given time
    and stored in the memory with the <code>load()</code> method. The XML will be de-serialized with the JAXB
    unmarshaller. A class with the exact XML format, that implements the <code>MapProvider</code> interface, must be
    provided.</dd>

  <dt>MemoryTranslationProvider</dt>
  <dd>The memory translation provider allows to add translations programmatically. The items can be added with the
    <code>addTranslation(String, String)</code> method.</dd>
</dl>

## Release Notes
### Version 1.0.0
First version that contains a eager JSON provider (<code>EagerJsonTranslationProvider</code>) and a programmatically
usable provider (<code>MemoryTranslationProvider</code>).

## Todo
* Add translation provider for plain text files (eager/lazy)
* Add translation provider for JSON files (lazy)
* Add translation provider for XML files (lazy)
* Add mechanism for caching translations.

## License
MIT