import ch.tsb.translation.StaticTranslationContext;
import ch.tsb.translation.TranslationContext;
import ch.tsb.translation.TranslationContextFactory;
import ch.tsb.translation.provider.MemoryTranslationProvider;

import static ch.tsb.translation.StaticTranslationContext.t;

/**
 * Example class for the {@link ch.tsb.translation.StaticTranslationContext} class.
 */
public class StaticContextExample {
  /**
   * Main entry point.
   *
   * @param args The program arguments.
   */
  public static void main(String[] args) {
    // setup a provider for german texts.
    final MemoryTranslationProvider german = new MemoryTranslationProvider();
    german.addTranslation("HELLO_WORLD", "Hallo Welt!");

    // setup a provider for english texts.
    final MemoryTranslationProvider english = new MemoryTranslationProvider();
    english.addTranslation("HELLO_WORLD", "Hello World!");

    // setup the translation context and add providers in the correct order.
    final TranslationContext context = TranslationContextFactory.createContext();
    context.addProvider(german);
    context.addProvider(english);

    // link the set up context to the static extension
    StaticTranslationContext.setContext(context);

    // use static extension for translation
    System.out.println(t("HELLO_WORLD"));
  }
}
