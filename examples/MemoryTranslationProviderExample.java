import ch.tsb.translation.TranslationContext;
import ch.tsb.translation.TranslationContextFactory;
import ch.tsb.translation.provider.MemoryTranslationProvider;

/**
 * Example class for the {@link ch.tsb.translation.provider.MemoryTranslationProvider} provider.
 */
public class MemoryTranslationProviderExample {
  /**
   * Main entry point.
   *
   * @param args The program arguments.
   */
  public static void main(String[] args) {
    // setup a provider for german texts.
    final MemoryTranslationProvider german = new MemoryTranslationProvider();
    german.addTranslation("HELLO_WORLD", "Hallo Welt!");

    // setup a provider for english texts.
    final MemoryTranslationProvider english = new MemoryTranslationProvider();
    english.addTranslation("HELLO_WORLD", "Hello World!");

    // setup the translation context and add providers in the correct order.
    final TranslationContext context = TranslationContextFactory.createContext();
    context.addProvider(german);
    context.addProvider(english);

    // translate the item.
    System.out.println(context.translate("HELLO_WORLD"));
  }
}
