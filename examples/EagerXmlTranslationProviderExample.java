import ch.tsb.translation.TranslationContext;
import ch.tsb.translation.TranslationContextFactory;
import ch.tsb.translation.provider.EagerXmlTranslationProvider;
import ch.tsb.translation.provider.MapProvider;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.util.Map;

/**
 * Example class for the {@link ch.tsb.translation.provider.EagerXmlTranslationProvider} provider.
 */
public class EagerXmlTranslationProviderExample {

  /**
   * Main entry point.
   *
   * @param args The program arguments.
   */
  public static void main(String[] args) {
    // setup the translation context and add providers in the correct order.
    final TranslationContext context = TranslationContextFactory.createContext();

    // setup a provider for german texts.
    try {
      context.addProvider(new EagerXmlTranslationProvider(new File("./examples/Example_de.xml"), TestMapProvider.class).load());
    } catch (JAXBException e) {
      System.out.println("Unable to load german texts");
    }

    // setup a provider for english texts.
    try {
      context.addProvider(new EagerXmlTranslationProvider(new File("./examples/Example_en.xml"), TestMapProvider.class).load());
    } catch (JAXBException e) {
      System.out.println("Unable to load english texts");
    }

    // translate the item.
    System.out.println(String.format("%s: %20s\n%s",
      context.translate("LANGUAGE"),
      context.translate("CUR_LANG"),
      context.translate("HELLO_WORLD")
    ));
  }

  /**
   * Class for de-serializing the examples xml format.
   */
  @XmlRootElement(name = "library")
  private static class TestMapProvider implements MapProvider {
    /**
     * The map that will be provided later.
     */
    public Map<String, String> translations;

    /**
     * {@inheritDoc}
     */
    @Override
    public Map getMap() {
      return translations;
    }
  }
}
