import ch.tsb.translation.TranslationContext;
import ch.tsb.translation.TranslationContextFactory;
import ch.tsb.translation.provider.EagerJsonTranslationProvider;

import java.io.File;
import java.io.IOException;

/**
 * Example class for the {@link ch.tsb.translation.provider.EagerJsonTranslationProvider} provider.
 */
public class EagerJsonTranslationProviderExample {

  /**
   * Main entry point.
   *
   * @param args The program arguments.
   */
  public static void main(String[] args) {
    // setup the translation context and add providers in the correct order.
    final TranslationContext context = TranslationContextFactory.createContext();

    // setup a provider for german texts.
    try {
      context.addProvider(new EagerJsonTranslationProvider(new File("./examples/Example_de.json")).load());
    } catch (IOException e) {
      System.out.println("Unable to load german texts");
    }

    // setup a provider for english texts.
    try {
      context.addProvider(new EagerJsonTranslationProvider(new File("./examples/Example_en.json")).load());
    } catch (IOException e) {
      System.out.println("Unable to load english texts");
    }

    // translate the item.
    System.out.println(String.format("%s: %20s\n%s",
      context.translate("LANGUAGE"),
      context.translate("CUR_LANG"),
      context.translate("HELLO_WORLD")
    ));
  }
}
