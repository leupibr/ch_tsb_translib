package ch.tsb.translation;

/**
 * Class that handles the providers and allows to translate items.
 */
public interface TranslationContext {
  /**
   * Adds a {@link TranslationProvider} to the context.
   * @param provider The provider to add.
   */
  void addProvider(final TranslationProvider provider);

  /**
   * Removes a {@link TranslationProvider} from the context.
   * @param provider The provider to remove.
   */
  void removeProvider(final TranslationProvider provider);

  /**
   * Gets the translation of an item. If it is not found, the <code>key</code> will be return.
   * @param key The key to lookup.
   * @return The translated text or the <code>key</code> if not found.
   */
  String translate(final String key);
}
