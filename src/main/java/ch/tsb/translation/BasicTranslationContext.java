package ch.tsb.translation;

import java.util.ArrayList;
import java.util.List;

/**
 * A basic implementation of the translation context.
 */
final class BasicTranslationContext implements TranslationContext {
  /**
   * The list of the translation providers.
   */
  private final List<TranslationProvider> providerList;

  /**
   * The holder for the lookup result.
   */
  private final StringBuilder resultHolder;

  /**
   * Initializes a new instance of the {@link ch.tsb.translation.BasicTranslationContext} class.
   */
  public BasicTranslationContext() {
    this.resultHolder = new StringBuilder();
    this.providerList = new ArrayList<>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addProvider(final TranslationProvider provider) {
    this.providerList.add(provider);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeProvider(final TranslationProvider provider) {
    this.providerList.remove(provider);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String translate(final String key) {
    synchronized (resultHolder) {
      // clear older results
      resultHolder.setLength(0);

      // try to get a value in the order of providers
      providerList
        .stream()
        .filter((TranslationProvider provider) -> provider.tryGetValue(key, resultHolder))
        .findFirst();

      if(resultHolder.length() > 0) {
        return resultHolder.toString();
      }
      return key;
    }
  }
}
