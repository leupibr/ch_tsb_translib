package ch.tsb.translation;

/**
 * Factory class for creating a translation context.
 */
@SuppressWarnings("WeakerAccess")
public final class TranslationContextFactory {

  /**
   * Creates a new {@link ch.tsb.translation.TranslationContext} instance.
   * @return The newly created translation context.
   */
  public static TranslationContext createContext() {
    return new BasicTranslationContext();
  }
}
