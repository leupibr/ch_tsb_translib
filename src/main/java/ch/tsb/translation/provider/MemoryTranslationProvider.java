package ch.tsb.translation.provider;

import ch.tsb.translation.TranslationProvider;

import java.util.HashMap;
import java.util.Map;

/**
 * A {@link ch.tsb.translation.TranslationProvider} that allows manual adding and removing of translations.
 */
public final class MemoryTranslationProvider implements TranslationProvider {

  /**
   * The translations of this provider.
   */
  private Map<String, String> translations;

  /**
   * Initializes a new instance of the {@link ch.tsb.translation.provider.MemoryTranslationProvider} class.
   */
  public MemoryTranslationProvider() {
    this.translations = new HashMap<>();
  }

  /**
   * Adds a new translation to the translations list.
   * @param key The key of the item that should be added.
   * @param text The text that should be added.
   */
  public void addTranslation(final String key, final String text) {
    this.translations.put(key, text);
  }

  /**
   * Removes an item from the translations list.
   * @param key The key of the item that should be removed.
   */
  public void removeTranslation(final String key) {
    this.translations.remove(key);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean tryGetValue(final String key, final StringBuilder translation) {
    if(this.translations.containsKey(key)) {
      translation.append(this.translations.get(key));
      return true;
    }
    return false;
  }
}
