package ch.tsb.translation.provider;

import ch.tsb.translation.TranslationProvider;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Map;

/**
 * A class that simply loads a XML file into a map.
 */
public final class EagerXmlTranslationProvider implements TranslationProvider {
  /**
   * The loaded translations.
   */
  private Map<String, String> translations;

  /**
   * The file that should be loaded.
   */
  private File fileToLoad;

  /**
   * The class that describes the XML structure.
   */
  private Class<? extends MapProvider> classType;

  /**
   * Initializes a new instance of the {@link ch.tsb.translation.provider.EagerXmlTranslationProvider} class.
   *
   * @param fileToLoad The file to load.
   */
  public EagerXmlTranslationProvider(final File fileToLoad, Class<? extends MapProvider> classType) {
    this.fileToLoad = fileToLoad;
    this.classType = classType;
  }

  /**
   * Loads the file into the hash map.
   *
   * @return The instance itself for method chaining.
   * @throws JAXBException if it was not possible to load the file.
   */
  public EagerXmlTranslationProvider load() throws JAXBException {
    final JAXBContext context = JAXBContext.newInstance(classType);
    final Unmarshaller unmarshaller = context.createUnmarshaller();

    // deserialize and get the map out.
    final MapProvider content = (MapProvider) unmarshaller.unmarshal(this.fileToLoad);
    this.translations = content.getMap();
    return this;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean tryGetValue(String key, StringBuilder translation) {
    if (this.translations.containsKey(key)) {
      translation.append(this.translations.get(key));
      return true;
    }
    return false;
  }
}
