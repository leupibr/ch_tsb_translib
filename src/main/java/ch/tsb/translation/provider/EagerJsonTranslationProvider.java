package ch.tsb.translation.provider;

import ch.tsb.translation.TranslationProvider;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A class that simply loads a JSON file into a map.
 */
public final class EagerJsonTranslationProvider implements TranslationProvider {
  /**
   * The loaded translations.
   */
  private Map<String, String> translations;

  /**
   * The file that should be loaded.
   */
  private File fileToLoad;

  /**
   * Initializes a new instance of the {@link ch.tsb.translation.provider.EagerJsonTranslationProvider} class.
   *
   * @param fileToLoad The file to load.
   */
  public EagerJsonTranslationProvider(final File fileToLoad) {
    this.fileToLoad = fileToLoad;
  }

  /**
   * Loads the file into the hash map.
   *
   * @return The instance itself for method chaining.
   * @throws IOException if it was not possible to load the file.
   */
  public EagerJsonTranslationProvider load() throws IOException {
    this.translations = new ObjectMapper().readValue(this.fileToLoad, HashMap.class);
    return this;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean tryGetValue(String key, StringBuilder translation) {
    if (this.translations.containsKey(key)) {
      translation.append(this.translations.get(key));
      return true;
    }
    return false;
  }
}
