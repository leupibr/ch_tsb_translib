package ch.tsb.translation.provider;

import java.util.Map;

/**
 * Interface for providing a map.
 */
public interface MapProvider {

  /**
   * Gets the map.
   *
   * @return The map.
   */
  Map getMap();
}
