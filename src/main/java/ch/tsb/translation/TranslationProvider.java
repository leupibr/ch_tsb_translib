package ch.tsb.translation;

/**
 * A translation provider, that provides translations for a context.
 */
@SuppressWarnings("WeakerAccess")
public interface TranslationProvider {

  /**
   * Tries to get a value from its source. The retrieved value will be added to the <code>translation</code> {@link java.lang.StringBuilder}.
   * @param key The key for the translation.
   * @param translation The {@link java.lang.StringBuilder} to append the translation.
   * @return <code>true</code> if the <code>key</code> was found, <code>false</code> otherwise.
   */
  boolean tryGetValue(final String key, final StringBuilder translation);
}
