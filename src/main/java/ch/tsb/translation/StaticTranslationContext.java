package ch.tsb.translation;

/**
 * Provides static extension for the translation library.
 */
public final class StaticTranslationContext {
  /**
   * The current translation context.
   */
  private static TranslationContext context;

  /**
   * Translates a key by using a given context. Use <code>setContext</code> for setting the context.
   * @param key The key of the item to translate.
   * @return The translated text or the <code>key</code> if no item was found or no context set.
   */
  public static String t(final String key) {
    // just return the key if no context is set.
    if(StaticTranslationContext.getContext() == null) {
      return key;
    }
    return StaticTranslationContext.getContext().translate(key);
  }

  /**
   * Sets the current translation context for the static extension.
   * @param context The context to set.
   */
  public static void setContext(TranslationContext context) {
    StaticTranslationContext.context = context;
  }

  /**
   * Gets the current translation context.
   */
  public static TranslationContext getContext() {
    return StaticTranslationContext.context;
  }
}
