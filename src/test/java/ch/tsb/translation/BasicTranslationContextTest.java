package ch.tsb.translation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Test class for the {@link ch.tsb.translation.BasicTranslationContext} class.
 */
public class BasicTranslationContextTest {
  /**
   * The subject that will be tested.
   */
  private TranslationContext context;

  /**
   * Sets up the mocks and the context.
   */
  @Before
  public void setUp() {
    // setup first provider
    final TranslationProvider providerOne = Mockito.mock(TranslationProvider.class);
    Mockito.when(providerOne.tryGetValue(Mockito.eq("EXISTING_ONE"), Mockito.anyObject())).thenAnswer(invocation -> {
      ((StringBuilder) invocation.getArguments()[1]).append("First element");
      return true;
    });
    Mockito.when(providerOne.tryGetValue(Mockito.eq("EXISTING_TWO"), Mockito.anyObject())).thenAnswer(invocation -> {
      ((StringBuilder) invocation.getArguments()[1]).append("Second element");
      return true;
    });

    // setup second provider
    final TranslationProvider providerTwo = Mockito.mock(TranslationProvider.class);
    Mockito.when(providerTwo.tryGetValue(Mockito.eq("EXISTING_THREE"), Mockito.anyObject())).thenAnswer(invocation -> {
      ((StringBuilder) invocation.getArguments()[1]).append("Third element");
      return true;
    });

    // setup context;
    context = new BasicTranslationContext();
    context.addProvider(providerOne);
    context.addProvider(providerTwo);
  }

  /**
   * Tests the {@link ch.tsb.translation.TranslationContext#translate(String)} method with an existing element at the first position of the first provider.
   */
  @Test
  public void testTranslate_ExistingFirstFirst() {
    Assert.assertEquals("First element", context.translate("EXISTING_ONE"));
  }

  /**
   * Tests the {@link ch.tsb.translation.TranslationContext#translate(String)} method with an existing element at the second position of the first provider.
   */
  @Test
  public void testTranslate_ExistingFirstSecond() {
    Assert.assertEquals("Second element", context.translate("EXISTING_TWO"));
  }

  /**
   * Tests the {@link ch.tsb.translation.TranslationContext#translate(String)} method with an existing element at the first position of the second provider.
   */
  @Test
  public void testTranslate_ExistingSecondFirst() {
    Assert.assertEquals("Third element", context.translate("EXISTING_THREE"));
  }

  /**
   * Tests the {@link ch.tsb.translation.TranslationContext#translate(String)} method with a non-existing element.
   */
  @Test
  public void testTranslate_NonExisting() {
    Assert.assertEquals("NON_EXISTING", context.translate("NON_EXISTING"));
  }
}