package ch.tsb.translation;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for the {@link ch.tsb.translation.TranslationContextFactory} class.
 */
public class TranslationContextFactoryTest {

  /**
   * Tests the {@link TranslationContextFactory#createContext()} method.
   */
  @Test
  public void testCreateContext() {
    Assert.assertThat(TranslationContextFactory.createContext(),
      CoreMatchers.instanceOf(BasicTranslationContext.class));
  }
}
