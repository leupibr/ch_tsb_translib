package ch.tsb.translation;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

/**
 * Test class for the {@link ch.tsb.translation.StaticTranslationContext} class.
 */
public class StaticTranslationContextTest {

  /**
   * Test method for the {@link ch.tsb.translation.StaticTranslationContext#t(String)} method.
   */
  @Test
  public void testT() {
    // setup context mock
    final TranslationContext context = Mockito.mock(TranslationContext.class);
    Mockito.when(context.translate(Mockito.eq("EXISTING_ONE"))).thenReturn("First item");

    // link context
    StaticTranslationContext.setContext(context);

    // test translation method
    Assert.assertEquals("First item", StaticTranslationContext.t("EXISTING_ONE"));
  }
}