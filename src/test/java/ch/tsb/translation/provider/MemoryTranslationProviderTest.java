package ch.tsb.translation.provider;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MemoryTranslationProviderTest {

  /**
   * Tests the {@link ch.tsb.translation.provider.MemoryTranslationProvider#addTranslation(String, String)} method.
   */
  @Test
  public void testAddTranslation() {
    final MemoryTranslationProvider provider = new MemoryTranslationProvider();
    provider.addTranslation("ITEM_ONE", "First item");
    provider.addTranslation("ITEM_TWO", "Second item");

    final StringBuilder builder = new StringBuilder();
    Assert.assertTrue(provider.tryGetValue("ITEM_ONE", builder));
    Assert.assertEquals("First item", builder.toString());

    builder.setLength(0);
    Assert.assertTrue(provider.tryGetValue("ITEM_TWO", builder));
    Assert.assertEquals("Second item", builder.toString());
  }

  /**
   * Tests the {@link ch.tsb.translation.provider.MemoryTranslationProvider#removeTranslation(String)} method.
   */
  @Test
  public void testRemoveTranslation() {
    final MemoryTranslationProvider provider = new MemoryTranslationProvider();
    provider.addTranslation("ITEM_ONE", "First item");
    provider.removeTranslation("ITEM_ONE");

    Assert.assertFalse(provider.tryGetValue("ITEM_ONE", new StringBuilder()));
  }
}