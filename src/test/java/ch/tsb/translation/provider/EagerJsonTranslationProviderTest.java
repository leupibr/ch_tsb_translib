package ch.tsb.translation.provider;

import ch.tsb.translation.TranslationProvider;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Test class for the {@link ch.tsb.translation.provider.EagerJsonTranslationProvider} class.
 */
public class EagerJsonTranslationProviderTest {
  /**
   * Temporary folder for the JSON files.
   */
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  /**
   * The file to use for loading.
   */
  private File file;

  /**
   * Setup before each test.
   *
   * @throws IOException if something goes wrong in file setup.
   */
  @Before
  public void setUp() throws IOException {
    this.file = this.folder.newFile("TestFile.json");
    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
    writer.write("{ \"ITEM_ONE\": \"First item\" }");
    writer.close();
  }

  /**
   * Cleanup after each test.
   */
  @After
  public void tearDown() {
    // cleanup folder.
    this.folder.delete();
  }

  /**
   * Tests the {@link EagerJsonTranslationProvider#load()} class.
   *
   * @throws IOException if something goes wrong while reading file.
   */
  @Test
  public void testLoad() throws IOException {
    final TranslationProvider provider = new EagerJsonTranslationProvider(this.file).load();

    final StringBuilder result = new StringBuilder();
    Assert.assertTrue(provider.tryGetValue("ITEM_ONE", result));
    Assert.assertEquals("First item", result.toString());
  }
}