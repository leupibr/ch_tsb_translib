package ch.tsb.translation.provider;

import ch.tsb.translation.TranslationProvider;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * Test class for the {@link EagerXmlTranslationProvider} class.
 */
public class EagerXmlTranslationProviderTest {
  /**
   * Temporary folder for the JSON files.
   */
  @Rule
  public TemporaryFolder folder = new TemporaryFolder();

  /**
   * The file to use for loading.
   */
  private File file;

  /**
   * Setup before each test.
   *
   * @throws java.io.IOException if something goes wrong in file setup.
   */
  @Before
  public void setUp() throws IOException {
    this.file = this.folder.newFile("TestFile.xml");
    final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
    writer.write(
      "<library><translations><entry><key>ITEM_ONE</key><value>First item</value></entry></translations></library>");
    writer.close();
  }

  /**
   * Cleanup after each test.
   */
  @After
  public void tearDown() {
    // cleanup folder.
    this.folder.delete();
  }

  /**
   * Tests the {@link ch.tsb.translation.provider.EagerXmlTranslationProvider#load()} class.
   *
   * @throws JAXBException if something goes wrong while reading file.
   */
  @Test
  public void testLoad() throws JAXBException {
    final TranslationProvider provider = new EagerXmlTranslationProvider(this.file, TestMapProvider.class).load();

    final StringBuilder result = new StringBuilder();
    Assert.assertTrue(provider.tryGetValue("ITEM_ONE", result));
    Assert.assertEquals("First item", result.toString());
  }

  /**
   * Class for de-serializing the test format.
   */
  @XmlRootElement(name = "library")
  private static class TestMapProvider implements MapProvider {
    /**
     * The translations that will be provided.
     */
    public Map<String, String> translations;

    /**
     * {@inheritDoc}
     */
    @Override
    public Map getMap() {
      return translations;
    }
  }
}